﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Packers
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        //[STAThread]
        //static void Main()
        //{
        //    Application.EnableVisualStyles();
        //    Application.SetCompatibleTextRenderingDefault(false);
        //    Application.Run(new Form1());
        //}

        //[STAThread]
        //static void Main(string[] args)
        //{
        //    Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Packers.easyIcon.exe");
        //    byte[] bs = new byte[stream.Length];
        //    stream.Read(bs, 0, (int)stream.Length);
        //    Assembly asm = Assembly.Load(bs);
        //    MethodInfo info = asm.EntryPoint;
        //    ParameterInfo[] parameters = info.GetParameters();
        //    if ((parameters != null) && (parameters.Length > 0))
        //        info.Invoke(null, (object[])args);
        //    else
        //        info.Invoke(null, null);
        //}

        [STAThread]
        static void Main(string[] args)
        {
            byte[] rar = Properties.Resources.dat.ToArray<byte>();
            Assembly.Load(rar).EntryPoint.Invoke(null, null);
        }
    }
}
